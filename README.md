# WISDome Project

Our amazing project in science engineering!

## Running

First, make sure you have all of the project's dependencies:

```console
npm install
```

You can run the app by running:

```console
npm start
```