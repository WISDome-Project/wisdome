/**
 * Created by iTooly on 3/30/2016.
 */

var express = require('express');
var router = express.Router();
var Gun = require('../models/gun');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../config'); // get our config file

router.get('/', function(req, res, next) {
    if(req.user){
        fetchByOwner(req.user._id, res, req);
    } else {
        res.redirect('/connect');
    }
});

router.post('/wps', function (req, res, next) {
   Gun.findOne({mac: req.body.mac}, function (err, gun) {
       if (err) { res.status(500).send(err); return; } 
       if (!gun) { res.status(404).send('No Gun Was Found!'); return; } 
       if (!gun.wps) { res.status(401).send('WPS is disabled!'); return; } 

       var token = jwt.sign({mac: req.body.mac, time: Date().now}, config.secret);
       /*Gun.findByIdAndUpdate(gun._id, {wps: true, token: token}, function (err, gun) {
           if (err) res.status(500).send(err);
           res.send('Hi Fuks');
           //res.status(200).send(token);
       })*/
       res.status(200).send(token);
   }) 
});

router.get('/new', function(req, res, next) {
    if(req.user){
        res.render('new_weapon', {data: { title: 'New Weapon', user: req.user}});
    } else {
        res.redirect('/connect');
    }
});

router.post('/new', function(req, res, next) {
    console.log(req.body);
    var gun = new Gun({
        owner       : req.user._id,
        name        : req.body.name,
        type        : req.body.type,
        mac         : req.body.mac,
        token       : jwt.sign({mac: req.body.mac, time: Date().now}, config.secret),
        plugged     : false,
        locations   : [],
        wps: true
    });

    gun.save(function (err, gun) {
        if(err)
            res.redirect('/connect');

        res.redirect('/');
    });
});

router.post('/shot', function (req, res, next) {
  res.status(200).send('Shot!');
});

function fetchByOwner(id, res, req) {
    Gun.find({owner: id}, function (err, guns) {
        if(err)
            return [];

        var new_guns = guns.map(function (o) {
            try {
              var decoded = jwt.verify(req.params.token, config.secret);
            } catch(err) {
              o.status = 'off';
              return o;
            }

            var delta = Math.floor(Date.now() / 1000) - Math.floor(decoded.time / 1000)

            if (delta > 1000)
                o.status = 'off';
            else
                o.status = 'on';

            if (o.plugged)
                o.status = 'plugged';

            return o;
        })

        res.render('weapons', {data: { title: 'Weapons', user: req.user, guns: new_guns }});
    });
}

module.exports = router;