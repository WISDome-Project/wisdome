var express = require('express');
var router = express.Router();

var Gun = require('../models/gun');
var Network = require('../models/network');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../config'); // get our config file

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('dashboard');
});

router.get('/connect', function (req, res, next) {
  res.render('connect', {data: { title: 'Connect' }});
});

router.get('/dashboard', function(req, res, next) {
  loadDasboard(req, res, next, 0);
});

router.get('/dashboard/:type/:id', function(req, res, next) {
  loadDasboard(req, res, next, req.params.id);
});

function loadDasboard(req, res, next, selected_gun) {
  if (!req.user) {
    res.render('dashboard', {data: { title: 'Dashboard', user: req.user}});
    return;
  }
  fetchGunsByOwner(req.user._id, function (guns) {

    fetchNetworksByOwner(req.user._id, function (networks) {
      var locationsP = guns[selected_gun].locations.map(function (o) {
        var p = new Promise((resolve, reject) => {
          Network.findOne(o._id, function (err, location) {
            if (err) reject(err);
            
            location.timestamp = o.timestamp;
            resolve(location);
          });
        })

        return p
      });

      Promise.all(locationsP).then(locations => {
        console.log(locations)
        res.render('dashboard', { data: { title: 'Dashboard', user: req.user, guns: guns, networks: networks, locations: locations } });
      })
    });
  });
}

router.get('/settings', function(req, res, next) {
  res.render('settings', { title: 'Settings', user: req.user });
});

router.post('/ping', function(req, res){
  // verifies secret and checks exp
  jwt.verify(req.body.token, config.secret, function(err, decoded) {
    if (err) {
      return res.status(401).json({ success: false, message: 'Failed to authenticate token.' });
    } else {
      // if everything is good, save to request for use in other routes
      Network.findOne({mac: req.body.bssid}, function (err, network) {
        Gun.update({mac: decoded.mac}, {$push: {locations: {_id: network._id, timestamp: Date.now()}}}, function (err, gun) {
          if (err) res.status(500).send(err);

          res.status(200).send('pong!');
        })
      })
    }
  });
});

function fetchGunsByOwner(id, callback) {
  Gun.find({owner: id}, function (err, guns) {
    if(err)
      return [];

    var new_guns = guns.map(function (o) {
      if (o.locations.length === 0) {
        o.status = 'off';
        return o;
      }
      
      var delta = Math.floor(Date.now()) - parseInt(o.locations[o.locations.length - 1].timestamp);
      if (delta > 5000)
        o.status = 'off';
      else
        o.status = 'on';

      if (o.plugged)
        o.status = 'plugged';

      return o;
    });

    callback(new_guns);
  });
}

function fetchNetworksByOwner(id, callback) {
  Network.find({owner: id}, function (err, networks) {
    callback(networks);
  });
}

function fetchNetworksById(id, callback) {
  Network.find({owner: id}, function (err, networks) {
    callback(networks);
  });
}

module.exports = router;
