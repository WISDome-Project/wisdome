/**
 * Created by iTooly on 3/31/2016.
 */

var express = require('express');
var router = express.Router();
var Network = require('../models/network');
var User = require('../models/user');

router.get('/', function(req, res, next) {
    if(req.user) {
        fetchByOwner(req.user._id, res, req);
    } else {
        res.redirect('/connect');
    }
});

router.get('/new', function(req, res, next) {
    if(req.user){
        res.render('new_network', {data: { title: 'New Network', user: req.user}});
    }
});

router.post('/new', function(req, res, next) {
    var emails = req.body.users.replace(/\s/g, '').split(',');
    var users = [];
    var count = 0;
    
    emails.forEach(function (email) {
        User.find({username: email}, function (err, user) {
            count++;
            if (!err)
                users.push(user._id);

            if (count == emails.length)
                addNetwork(res, users, req.body, req.user);
        });
    })

    /*
    var network = new Network({
        owner   : req.user._id,
        name    : req.body.name,
        type    : req.body.type,
        mac     : req.body.mac,
        users   : [req.user._id]
    gun.save(function (err, gun) {
        if(err)
            res.redirect('/connect');
        res.redirect('/');
    })
    */
})

function addNetwork(res, users, args, owner) {
    var network = new Network({
        owner   : owner._id,
        name    : args.name,
        type    : args.type,
        mac     : args.mac,
        users   : users
    });
    network.save(function (err, gun) {
        if(err)
            res.redirect('/connect');
        res.redirect('/');
    })
}

function fetchByOwner(id, res, req) {
    Network.find({owner: id}, function (err, networks) {
        res.render('networks', {data: { title: 'Networks', user: req.user, networks: networks }});
    });
}

module.exports = router;